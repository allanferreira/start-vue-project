import Home from './components/home/Home.vue'
import Sobre from './components/sobre/Sobre.vue'

export const routes = [
  {
    path: '',
    component: Home,
    titulo: 'Home'
  },
  {
    path: '/sobre',
    component: Sobre,
    titulo: 'Sobre'
  }
]
