const gulp = require('gulp')

gulp.task('statics', function buildHTML() {
    return gulp.src('statics/**/*')
        .pipe(gulp.dest('./public_html/statics'))
})
gulp.task('dist', function buildHTML() {
    return gulp.src('dist/**/*')
        .pipe(gulp.dest('./public_html/dist'))
})
gulp.task('index', function buildHTML() {
    return gulp.src('./index.html')
        .pipe(gulp.dest('./public_html'))
})

gulp.task('deploy', ['statics', 'dist', 'index'])
gulp.task('default', ['deploy'])
